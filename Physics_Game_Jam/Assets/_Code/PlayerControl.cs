﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public Rigidbody rb;
    public Rigidbody rb2;
    public float Force = 10;
    public int PlayerID;

    public 
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb2 = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        ControlPlayer();
        ControlPlayer2();
        ResetPosition();
    }

    void ControlPlayer()
    {
    if (Input.GetKeyDown(KeyCode.LeftArrow) && PlayerID == 1)
        {
        rb.AddForce(Vector3.left * Force * 10, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && PlayerID == 1)
        {
            rb.AddForce(Vector3.right * Force * 10, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) && PlayerID == 1)
        {
            rb.AddForce(Vector3.down * Force * 10, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && PlayerID == 1)
        {
            rb.AddForce(Vector3.up * Force * 10, ForceMode.Impulse);
        }
    }

    void ControlPlayer2()
    {
        if (Input.GetKeyDown(KeyCode.A) && PlayerID == 1)
        {
            rb2.AddForce(Vector3.left * Force * 10, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.D) && PlayerID == 1)
        {
            rb2.AddForce(Vector3.right * Force * 10, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.S) && PlayerID == 1)
        {
            rb2.AddForce(Vector3.down * Force * 10, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.W) && PlayerID == 1)
        {
            rb2.AddForce(Vector3.up * Force * 10, ForceMode.Impulse);
        }
    }

    void ResetPosition()
    {
        if (Input.GetKeyDown(KeyCode.R) && PlayerID == 1)
        {
            
        }
            

    }


}
