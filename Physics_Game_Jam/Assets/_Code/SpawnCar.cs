﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCar : MonoBehaviour
{

    public GameObject[] obstaclePrefab;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SpawnObstacle", 0.5f);
    }

    // Update is called once per frame
    void SpawnObstacle()
    {
        int rand = Random.Range(0, obstaclePrefab.Length);

        GameObject go = Instantiate(obstaclePrefab[rand],
            new Vector3(0, 0, 25),
            Quaternion.identity);
        float nextSpawnTime = Random.Range(1.5f, 2f);
        Invoke("SpawnObstacle", nextSpawnTime);     
        Destroy(go, 10.0f);
    }

}
