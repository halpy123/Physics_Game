﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    public GameObject[] obstaclePrefab;
    public float spawnTime = 5;
    private float timer = 1;
    
    void Update()
    {
        //if (Mathf.Round(timer) % 2 == 0 )
        if (timer > spawnTime)
        {
            int rand = Random.Range(0, obstaclePrefab.Length);

            GameObject obs = Instantiate(obstaclePrefab[rand]);
            obs.transform.position = transform.position = new Vector3(0, 1.75f, 17);
            Destroy(obs, 5);
        }

        timer += Time.deltaTime;
    }
}
