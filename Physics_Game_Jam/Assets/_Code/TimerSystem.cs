﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSystem : MonoBehaviour
{
    public static float TimeStart = 0;
    public Text textbox;

    public Text _currentScore;
    public Text _bestScore;

    // Start is called before the first frame update
    void Start()
    {
        textbox.text = TimeStart.ToString();
        _bestScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    // Update is called once per frame
    void Update()
    {       
           TimeStart += Time.deltaTime;
           textbox.text = Mathf.Round(TimeStart).ToString();
        if (TimeStart > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", Mathf.FloorToInt(TimeStart));
            _bestScore.text = TimeStart.ToString();
        }

        _currentScore.text = Mathf.Round(TimeStart).ToString();
    } 
}

    

