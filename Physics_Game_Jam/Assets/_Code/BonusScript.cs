﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            TimerSystem.TimeStart += 5;
            FindObjectOfType<AudioManager>().Play("Bonus");
        }
    }
}
