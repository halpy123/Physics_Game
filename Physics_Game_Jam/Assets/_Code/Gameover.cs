﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gameover : MonoBehaviour
{
    private bool isGameOver = false;
    public GameObject Dashborad;

    public void Endgame()
    {
        Time.timeScale = 0;
        isGameOver = true;
        Dashborad.SetActive(isGameOver);
    }

    public void RestartGame()
    {
        isGameOver = false;
        Time.timeScale = 1;
        TimerSystem.TimeStart = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Dashborad.SetActive(isGameOver);
    }
}
